#!/bin/bash
# Wrapper script for Mkdocs Docker container
# Function to create a new Mkdocs project
function create () { 

    local project_name="$1"

    # Create a new Mkdocs project

    mkdocs new "$project_name"
    sed -i 's/\r//' "$project_name/mkdocs.yml"

}



function produce () { 

    local input_directory="$1"

	# Create a tar.gz file containing the Mkdocs project files
    echo "Producing $input_directory to website.tar.gz"
    cd "$input_directory"
    mkdocs build
	tar czvf website.tar.gz -C "$input_directory/site" .

}



function serve () { 

    # Use Mkdocs to serve the website from stdin

    # Create a temporary directory
    temp_dir=$(mktemp -d)
    mkdir -p "$temp_dir"
    # Read the tar.gz file from stdin and extract it to the temporary directory
    cat | tar -xzf - -C "$temp_dir"
    mkdir -p "/var/www/mkdocs"
    cp -r $temp_dir/* /var/www/mkdocs
    cd /var/www/mkdocs
    rm -r "$temp_dir"
    python -m http.server -d /var/www/mkdocs 8000
    # mkdocs serve -f extracted-website/mkdocs.yml -a 0.0.0.0:8000
    # mkdocs serve -f /dev/stdin  -a 0.0.0.0:8000

}



# Main script execution
case "$1" in
    
    create)
        shift
        create "$@"
        ;;
    produce)
        shift
        produce "$@"
        ;;
    serve)
        shift
        serve
        ;;
    *)
        echo "Usage: $0 {create|produce|serve}"
        # exit 1
        bash -c "$*"
        ;;
esac
