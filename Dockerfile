# Use an official Python runtime as a parent image
FROM python:3.11-slim

# Set the working directory to /app
WORKDIR /app

# Install Mkdocs and dependencies
RUN pip install mkdocs

# Copy the current directory contents into the container at /app
COPY . /app

# Copy the entrypoint script into user bin
COPY mkdockerize.sh /usr/local/bin/mkdockerize

# RUN dos2unix mkdockerize.sh
RUN sed -i 's/\r//' /usr/local/bin/mkdockerize

# Make the wrapper script executable
RUN chmod +x /usr/local/bin/mkdockerize

# Expose port 8000
EXPOSE 8000


# Run wrapper script by default
ENTRYPOINT ["bash", "/usr/local/bin/mkdockerize"]
